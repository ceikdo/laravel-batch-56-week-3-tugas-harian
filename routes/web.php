<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/biodata', [HomeController::class, 'biodata']);

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

// Create -> Form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// Untuk menambah data ke database
Route::post('/cast', [CastController::class, 'store']);

// Read -> Menampilkan semua data
Route::get('/cast', [CastController::class, 'index']);
// Menampilkan detail cast berdasarkan index
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update -> Mengubah data
// Form update data
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// Update data ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete -> Menghapus data
// Delete data di database berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

