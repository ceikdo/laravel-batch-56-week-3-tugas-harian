@extends('layout.master')

@section('judul')
Halaman Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
    <label>Nama Lengkap</label><br>
    <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control"><br>
    @error('nama')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <label>Umur</label><br>
    <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control"><br>
    @error('umur')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <label>Bio</label><br>
    <textarea name="bio" id="" cols="30" rows="10">{{ $cast->bio }}</textarea><br>
    @error('bio')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <input type="submit" class="btn btn-primary btn-sm">
</form>
@endsection
