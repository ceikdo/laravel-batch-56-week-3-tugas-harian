@extends('layout.master')

@section('judul')
Halaman Cast Film
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <label>Nama Lengkap</label><br>
    <input type="text" name="nama" id="nama" placeholder="Masukkan Nama"><br>
    @error('nama')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <label>Umur</label><br>
    <input type="number" name="umur" id="umur"><br>
    @error('umur')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <label>Bio</label><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
    @error('bio')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <input type="submit" class="btn btn-primary btn-sm">
</form>
@endsection
